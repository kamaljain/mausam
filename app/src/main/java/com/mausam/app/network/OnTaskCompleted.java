package com.mausam.app.network;

public interface OnTaskCompleted {
    void onTaskCompleted(Object obj, RequestObject requestObject);
}