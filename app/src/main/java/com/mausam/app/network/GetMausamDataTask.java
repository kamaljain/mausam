package com.mausam.app.network;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;

import com.kaopiz.kprogresshud.KProgressHUD;
import com.mausam.app.AppDelegate;
import com.mausam.app.R;
import com.mausam.app.model.MausamData;

import org.springframework.http.converter.HttpMessageConversionException;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;


public class GetMausamDataTask extends AsyncTask<Void, Void, MausamData> {


    String cityId;
    OnTaskCompleted onTaskCompleted;
    KProgressHUD kProgressHUD = null;
    Context a;

    public GetMausamDataTask(Context a, String cityId, OnTaskCompleted onTaskCompleted) {
        this.cityId = cityId;
        this.a = a;
        this.onTaskCompleted = onTaskCompleted;

    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        kProgressHUD = KProgressHUD.create(a)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();
    }


    @Override
    protected MausamData doInBackground(Void... params) {
        RestTemplate rt = new RestTemplate();
        rt.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
        try {
            String url = a.getString(R.string.server_url).replace("{cityId}", cityId).replace("{appid}", a.getString(R.string.app_id));
            return rt.getForObject(url, MausamData.class);

        } catch (HttpClientErrorException e) {
            e.printStackTrace();
            return null;
        } catch (HttpMessageConversionException e) {
            e.printStackTrace();
            return null;
        } catch (RestClientException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    protected void onPostExecute(MausamData mausamData) {
        if (kProgressHUD != null && kProgressHUD.isShowing()) {
            kProgressHUD.dismiss();
        }
        if(mausamData!=null) {
            AppDelegate.setMausamData(mausamData);
        }
        if(onTaskCompleted!=null) {
            onTaskCompleted.onTaskCompleted(mausamData, RequestObject.GET_DATA);
        }
    }
}