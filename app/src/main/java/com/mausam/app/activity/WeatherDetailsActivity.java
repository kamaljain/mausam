package com.mausam.app.activity;

import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.mausam.app.AppDelegate;
import com.mausam.app.R;
import com.mausam.app.model.DataList;
import com.mausam.app.util.PrefUtils;
import com.mausam.app.util.UnitConvertor;
import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;

public class WeatherDetailsActivity extends AppCompatActivity {

    @BindView(R.id.itemTemperature)
    public TextView itemTemperature;
    @BindView(R.id.itemDescription)
    public TextView itemDescription;
    @BindView(R.id.itemWind)
    public TextView itemyWind;
    @BindView(R.id.itemPressure)
    public TextView itemPressure;
    @BindView(R.id.itemHumidity)
    public TextView itemHumidity;
    @BindView(R.id.itemIcon)
    public ImageView itemIcon;
    @BindView(R.id.itemDate)
    TextView itemDate;

    DataList weatherItem;
    private Toolbar mToolbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        ButterKnife.bind(this);

        setTitle("Details");

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        weatherItem = AppDelegate.getDataList();
        if(weatherItem== null){
            return;
        }

        setupData();

        
    }
    
    public void setupData(){
        SharedPreferences sp = PrefUtils.getSharedPreferences();

        // Temperature
        float temperature = UnitConvertor.convertTemperature(Float.parseFloat(weatherItem.getTemp().getDay() + ""), sp);
        if (sp.getBoolean("temperatureInteger", false)) {
            temperature = Math.round(temperature);
        }

        // Rain
        double rain = Double.parseDouble(weatherItem.getClouds() + "");
        String rainString = UnitConvertor.getRainString(rain, sp);

        // Wind
        double wind;
        try {
            wind = Double.parseDouble(weatherItem.getSpeed() + "");
        } catch (Exception e) {
            e.printStackTrace();
            wind = 0;
        }
        wind = UnitConvertor.convertWind(wind, sp);

        // Pressure
        double pressure = UnitConvertor.convertPressure((float) Double.parseDouble(weatherItem.getPressure() + ""), sp);

        TimeZone tz = TimeZone.getDefault();
        String defaultDateFormat = getResources().getStringArray(R.array.dateFormatsValues)[0];
        String dateFormat = sp.getString("dateFormat", defaultDateFormat);
        if ("custom".equals(dateFormat)) {
            dateFormat = sp.getString("dateFormatCustom", defaultDateFormat);
        }
        String dateString;
        try {
            SimpleDateFormat resultFormat = new SimpleDateFormat(dateFormat);
            resultFormat.setTimeZone(tz);
            dateString = resultFormat.format(new Date(((long)weatherItem.getDt())*1000));
        } catch (IllegalArgumentException e) {
            dateString = getResources().getString(R.string.error_dateFormat);
        }

        itemDate.setText(dateString);

        if (sp.getBoolean("displayDecimalZeroes", false)) {
            itemTemperature.setText(new DecimalFormat("0.0").format(temperature) + " " + sp.getString("unit", "°C"));
        } else {
            itemTemperature.setText(new DecimalFormat("#.#").format(temperature) + " " + sp.getString("unit", "°C"));
        }

        itemDescription.setText(weatherItem.getWeather().get(0).getDescription().toUpperCase() +
                rainString);


        Picasso.with(WeatherDetailsActivity.this)
                .load(getString(R.string.cdn_url).replace("{icon}",weatherItem.getWeather().get(0).getIcon()))
                .fit()
                .centerCrop()
                .into(itemIcon);


        if (sp.getString("speedUnit", "m/s").equals("bft")) {
            itemyWind.setText( getString(R.string.wind) + ": " +
                    UnitConvertor.getBeaufortName((int) wind));
        } else {
            itemyWind.setText( getString(R.string.wind) + ": " + new DecimalFormat("0.0").format(wind) + " ");
        }
        itemPressure.setText( getString(R.string.pressure) + ": " + new DecimalFormat("0.0").format(pressure));
        itemHumidity.setText( getString(R.string.humidity) + ": " + weatherItem.getHumidity() + " %");


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }

        return super.onOptionsItemSelected(item);
    }
}
