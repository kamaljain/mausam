package com.mausam.app.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;

import com.mausam.app.R;

public class SplashActivity extends Activity implements View.OnTouchListener {

    private Runnable runner;
    private Handler runnerHandler;
    private long SPLASH_TIME = 2000;

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        finishSplash();
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_splash);

    }

    @Override
    protected void onStart() {
        super.onStart();
        runnerHandler = new Handler();
        runner = new Runnable() {
            @Override
            public void run() {
                finishSplash();
            }
        };
        runnerHandler.postDelayed(runner, SPLASH_TIME);

    }

    private void finishSplash() {
        if (isFinishing()) {
            return;
        }
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }
}