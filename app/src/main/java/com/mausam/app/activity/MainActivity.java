package com.mausam.app.activity;

import android.annotation.TargetApi;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.RequiresApi;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.mausam.app.AppDelegate;
import com.mausam.app.R;
import com.mausam.app.adapter.WeatherRecyclerAdapter;
import com.mausam.app.model.MausamData;
import com.mausam.app.network.GetMausamDataTask;
import com.mausam.app.network.OnTaskCompleted;
import com.mausam.app.network.RequestObject;

import com.mausam.app.service.JobSchedulerService;
import com.mausam.app.util.RecyclerItemClickListener;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements OnTaskCompleted {
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    MausamData mausamData=null;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setLogo(R.mipmap.ic_launcher);
        actionBar.setDisplayUseLogoEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);

        if(AppDelegate.getStoredValue("city")== null){
            AppDelegate.storeValue("city",getResources().getStringArray(R.array.city_ids)[0]);
        }

        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        recyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(MainActivity.this, recyclerView ,new RecyclerItemClickListener.OnItemClickListener() {
                    @Override public void onItemClick(View view, int position) {

                        Intent intent = new Intent(MainActivity.this, WeatherDetailsActivity.class);
                        AppDelegate.setDataList(mausamData.getList().get(position));
                        startActivity(intent);
                    }

                    @Override public void onLongItemClick(View view, int position) {

                    }
                })
        );
        setUpAlarmData();

    }

    @TargetApi(Build.VERSION_CODES.N)
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    void setUpAlarmData(){
        try {
            JobScheduler jobScheduler = (JobScheduler) getApplicationContext()
                    .getSystemService(JOB_SCHEDULER_SERVICE);
            ComponentName componentName = new ComponentName(this,
                    JobSchedulerService.class);

            JobInfo jobInfoObj = new JobInfo.Builder(1, componentName)
                    .setPeriodic(15 * 60 * 1000, 5 * 60 * 1000).build();
            int resultCode = jobScheduler.schedule(jobInfoObj);
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        handler.postDelayed(runnable, 1000);

    }

    final Handler handler = new Handler();
    Runnable runnable = new Runnable() {

        @Override
        public void run() {
            try{
                if(AppDelegate.isNetworkAvailable()) {
                     new GetMausamDataTask(MainActivity.this, AppDelegate.getStoredValue("city"), MainActivity.this).execute();
                }else{
                    Toast.makeText(MainActivity.this, R.string.internet_connection, Toast.LENGTH_SHORT).show();
                    mausamData = AppDelegate.getMausamData();
                    WeatherRecyclerAdapter weatherRecyclerAdapter = new WeatherRecyclerAdapter(MainActivity.this, mausamData.getList());
                    recyclerView.setAdapter(weatherRecyclerAdapter);
                }

            }
            catch (Exception e) {
                // TODO: handle exception
            }
            finally{
                //also call the same runnable to call it at regular interval
                handler.postDelayed(this, 15000);
            }
        }
    };

    @Override
    public void onTaskCompleted(Object obj, RequestObject requestObject) {
        if (obj != null) {
            switch (requestObject) {
                case GET_DATA:
                    mausamData = (MausamData) obj;
                    AppDelegate.setMausamData(mausamData);
                    WeatherRecyclerAdapter weatherRecyclerAdapter = new WeatherRecyclerAdapter(this, mausamData.getList());
                    recyclerView.setAdapter(weatherRecyclerAdapter);
                    break;
            }
        } else {
            Toast.makeText(this, R.string.try_again, Toast.LENGTH_SHORT).show();
            mausamData = AppDelegate.getMausamData();
            WeatherRecyclerAdapter weatherRecyclerAdapter = new WeatherRecyclerAdapter(this, mausamData.getList());
            recyclerView.setAdapter(weatherRecyclerAdapter);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:

                Intent intent = new Intent(MainActivity.this, SettingsActivity.class);
                startActivity(intent);
                break;
        }
        return true;

    }


}
