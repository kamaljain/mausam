package com.mausam.app.service;

import android.app.job.JobParameters;
import android.app.job.JobService;
import android.os.Build;
import android.support.annotation.RequiresApi;

import com.mausam.app.AppDelegate;
import com.mausam.app.R;
import com.mausam.app.network.GetMausamDataTask;

@RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
public class JobSchedulerService extends JobService {

    @Override
    public boolean onStartJob(JobParameters params) {
        new GetMausamDataTask(getApplicationContext(), AppDelegate.getStoredValue("city"), null).execute();
        return false;
    }

    @Override
    public boolean onStopJob(JobParameters params) {

        return false;
    }

}