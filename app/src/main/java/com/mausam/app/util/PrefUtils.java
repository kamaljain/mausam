package com.mausam.app.util;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.mausam.app.AppDelegate;


public class PrefUtils {

    public static SharedPreferences getSharedPreferences() {
        return PreferenceManager.getDefaultSharedPreferences(AppDelegate.getContext());
    }

    public static void clearPreference() {
        SharedPreferences sharedPreferences = getSharedPreferences();
        sharedPreferences.edit().clear();

    }
}
