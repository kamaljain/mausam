package com.mausam.app;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.mausam.app.model.DataList;
import com.mausam.app.model.MausamData;
import com.mausam.app.util.PrefUtils;

import org.acra.ACRA;
import org.acra.ReportingInteractionMode;
import org.acra.annotation.ReportsCrashes;

import java.util.ArrayList;

@ReportsCrashes(formKey = "", // will not be used
        mailTo = "kamaljain1008@gmail.com", resDialogText = R.string.submit_crash, mode = ReportingInteractionMode.DIALOG, resToastText = R.string.crash_toast_text, resNotifTickerText = R.string.crash_notif_ticker_text, resNotifTitle = R.string.crash_notif_title, resNotifText = R.string.crash_notif_text, resNotifIcon = R.mipmap.ic_launcher, resDialogIcon = android.R.drawable.ic_dialog_info, resDialogTitle = R.string.crash_dialog_title, resDialogCommentPrompt = R.string.crash_dialog_comment_prompt, resDialogOkToast = R.string.crash_dialog_ok_toast)
public class AppDelegate extends MultiDexApplication {

    public static boolean language = true;

    private static Context context;

    public static Context getContext() {
        return context;
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);

    }

    @Override
    public void onCreate() {
        super.onCreate();
        context = getApplicationContext();
        MultiDex.install(this);
        ACRA.init(this);

    }

    public static DataList getDataList() {
        SharedPreferences preferences = PrefUtils.getSharedPreferences();

        Gson gson = new GsonBuilder().create();

        String str = preferences.getString("DataList", null);
        if (str != null) {
            return gson.fromJson(str, new TypeToken<DataList>() {
            }.getType());
        }
        return null;
     }

    public static void setDataList(DataList datalist) {
        Gson gson = new GsonBuilder().create();
        SharedPreferences preferences = PrefUtils.getSharedPreferences();
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("DataList", gson.toJson(datalist));
        editor.commit();
    }


    public static String getStoredValue(String key) {
        SharedPreferences preferences = PrefUtils.getSharedPreferences();
        return preferences.getString(key, null);
     }

    public static void storeValue(String key,String value) {

        SharedPreferences preferences = PrefUtils.getSharedPreferences();
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public static MausamData getMausamData() {
        SharedPreferences preferences = PrefUtils.getSharedPreferences();

        Gson gson = new GsonBuilder().create();

        String str = preferences.getString("MausamData", null);
        if (str != null) {
            return gson.fromJson(str, new TypeToken<MausamData>() {
            }.getType());
        }
        return null;
    }

    public static void setMausamData(MausamData mausamData) {
        Gson gson = new GsonBuilder().create();
        SharedPreferences preferences = PrefUtils.getSharedPreferences();
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("MausamData", gson.toJson(mausamData));
        editor.commit();
    }

    public static boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}
